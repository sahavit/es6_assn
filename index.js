var gridNum = 0;
var imageList = [];

function displayItems(){
    data.forEach(item => {
        gridNum++;
        let idName="main-grid-"+gridNum;
        if(item.contentName=="Carousel"){
            document.getElementById("main").innerHTML+=
            "<div id='car'></div>"+
            "<div class='rect'><svg><rect width='100%' height='5' style=fill:#575656;'/></svg></div>";
            item.content.forEach(element => {
                let filteredList = []
                filteredList=element.images.filter(image => image.type=="system" && image.format=="original")
                imageList.push(filteredList[0]);
            })
        }
        else{
            document.getElementById("main").innerHTML+=
            "<div id='title'>"+item.contentName+"</div>"+
            "<div id='"+idName+"' class='main-grid'></div>"+
            "<div class='rect'><svg><rect width='100%' height='5' style=fill:#575656;'/></svg></div>";
            item.content.forEach(element => {
                let filteredList = []
                filteredList=element.images.filter(image => image.type=="cover" && image.format=="thumbnail-hd")
                document.getElementById(idName).innerHTML+=
                "<div class='nested-grid'>"+
                "<div><img src='"+filteredList[0].url+"'></div>"+
                "<div id='txt'><input hidden title='"+element.title+"'>"+element.title+"</div></div>" 
            });
        }
    });
}

function displayCarousel(){
    var i = 0;
    var time = 5000;
    document.getElementById("car").innerHTML=
    "<img src='"+imageList[0].url+"'>";
    
    imageList.forEach(element => {
        i++;
        let totalTime = time*i;
        setInterval( () => {
            document.getElementById("car").innerHTML=
            "<img src='"+element.url+"'>";
        }, totalTime)
    });

}

displayItems()
displayCarousel()

// Function without carousel

// var gridNum = 0;
// data.forEach(item => {
//     gridNum++;
//     let idName="main-grid-"+gridNum;
//     document.getElementById("main").innerHTML+=
//     "<div id='title'>"+item.contentName+"</div>"+
//     "<div id='"+idName+"' class='main-grid'></div>"+
//     "<div class='rect'><svg><rect width='100%' height='5' style=fill:#575656;'/></svg></div>";
//     item.content.forEach(element => {
//         let filteredList = []
//         filteredList=element.images.filter(image => image.type=="cover" && image.format=="thumbnail-hd")
//         console.log(filteredList)
//         document.getElementById(idName).innerHTML+=
//         "<div class='nested-grid'>"+
//         "<div><img src='"+filteredList[0].url+"'></div>"+
//         "<div id='txt'><input hidden title='"+element.title+"'>"+element.title+"</div></div>" 
//     });
// });